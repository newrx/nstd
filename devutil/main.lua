function loadgame()
  local loveclone = love

  function love.keyreleased(key, scancode)
    if love.keyboard.isDown('lgui') and key == 'r' then
      print('hot reloading application...')
      -- require('app')
      loadgame()

      print('done.')
    end
  end

  local childproc = loadfile('game/main.lua')
  setfenv(childproc, {
    love = loveclone,
    print = print,
    require = require,
    table = table,
    pairs = pairs,
    ipairs = ipairs,
    tostring = tostring,
    os = os,
    process = {
      env = "dev",
    },
  })

  childproc()
end

loadgame()
