game:
	/Applications/love.app/Contents/MacOS//love

release:
	-rm -r bin
	mkdir -p bin/dist
	cp -r build/macos/NoSleepTilDawn.app bin/dist/NoSleepTilDawn.app
	cp -r ${PWD}/game bin/dist/NoSleepTilDawn.app/Contents/MacOS/gamedir

# buildwin:
# 	cat love.exe SuperGame.love > SuperGame.exe
