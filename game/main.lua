local scenegraph = require("scenegraph")
local templateScene = require("scenes/template")
local helloScene = require("scenes/hello")

function love.load()
  stage = scenegraph.createStage()
  stage:load()

  stage:setScene(helloScene)
end

function love.update(dt)
  stage:update(dt)
end

function love.draw()
  stage:draw()
end

function love.mousepressed()
  stage:setScene(templateScene)
end
