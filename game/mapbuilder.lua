local json = require("lib/json")
local inspect = require("lib/inspect")

local ONE_PI = 1
local TWO_PI = 2
local THREE_PI = 3

-- tiles
local tile = {
  id = 1,
  drawable = "quad",
  rotation = 0 -- num of quarter pi/45 degree rotations
}

function loadBundle(filepath)
  contents, size = love.filesystem.read(filepath.."/manifest.json")
  return {
    filepath = filepath,
    manifest = json.decode(contents),
  }
end

function processAtlas(img, tileSize, tileCount)
  width, height = img:getDimensions()
  local quads = {}

  local tilesLong = width / tileSize.x
  local x = 0
  local y = 0

  for i = 0, tileCount-1, 1 do
    local atRowEnd = x + tileSize.x == width
    quad = love.graphics.newQuad(
      x,
      y,
      tileSize.x,
      tileSize.y,
      width,
      height
    )

    table.insert(quads, quad)
    if atRowEnd then
      x = 0
      y = y + tileSize.y
    else
      x = x + tileSize.x
    end
  end

  return quads
end

function mapTileToDrawable(drawableSet, tileSet, rotation)
  rotation = rotation or 0
  local mapped = {}

  for i = 1, #tileSet, 1 do
    local tile = tileSet[i]

    table.insert(mapped, {
      id = i,
      drawable = drawableSet[tile[1]],
      rotation = tile[2],
    })
  end

  return mapped
end

function generateMap(bundle)
  atlas = bundle.filepath.."/"..bundle.manifest.atlas.file

  atlasImg = love.graphics.newImage(atlas)
  atlasImg:setFilter("nearest", "nearest")

  quadSet = processAtlas(atlasImg, bundle.manifest.atlas.tileSize, bundle.manifest.atlas.tileCount)
  tileSet = mapTileToDrawable(quadSet, bundle.manifest.grid.tiles)

  return newGrid(atlasImg, tileSet, {x = bundle.manifest.grid.width, y = bundle.manifest.grid.height})
end

-- importAssets = function(filepath)
--
--       -- atlas = love.graphics.newImage(filepath.."/"..map.atlas.file)
--       -- atlas:setFilter("nearest", "nearest")
--       --
--       -- width, height = atlas:getDimensions()
--       -- local quads = generateQuadSet(width, height, map.atlas.tileSize, map.atlas.tileCount)
--       --
--       -- local mapwidth = map.grid.width
--       -- local mapheight = map.grid.height
-- end

function newGrid(atlas, tiles, size)
  if not tiles then
    tiles = {}
    for i = 1, 16, 1 do
      table.insert(tiles, nil)
    end
  end

  local grid = {
    showlines = false,
    atlas = atlas,

    scalefx = {
      x = 6,
      y = 6,
    },

    size = size,
    tile = {
      size = {
        x = 8,
        y = 8,
      },
    },

    tiles = tiles,
  }

  function grid.export(self)
    local ids = {}
    for i = 1, self.tiles, 1 do
      table.insert(ids, {id = self.tiles[i].id, rotation = self.tiles[i].rotation})
    end

    return ids
  end

  function grid.getSizePx(self)
    return (self.size.x * self.tile.size.x) * self.scalefx.x, (self.size.y * self.tile.size.y) * self.scalefx.y
  end

  function grid.setTile(self, tilePos, tile)
    self.tiles[tilePos] = tile
  end

  function grid.getTile(self, tilePos)
    return self.tiles[tilePos]
  end

  function grid.toggleLines(self)
    self.showlines = not self.showlines
  end

  function grid.toggleNumberLabels(self)
    self.shownumbers = not self.shownumbers
  end

  function grid.getTilePos(self, x, y)
    -- translate to grid 0
    x = x - self.position.x
    y = y - self.position.y

    -- check bounds
    local width, height = self:getSizePx()
    if x < 0 or y < 0 or x > width or y > height then
      return nil
    end

    x = x / self.scalefx.x
    y = y / self.scalefx.y

    local xpos = math.ceil(x / self.tile.size.x)
    local ypos = math.ceil(y/self.tile.size.y)

    return xpos + ((ypos - 1) * self.size.x)
  end

  function grid.getBounds()

  end

  function grid.draw(self, wx, wy)
    love.graphics.push()
    love.graphics.translate(wx, wy)

    local x = 0
    local y = 0

    -- clear color buffer
    love.graphics.setColor(255, 255, 255, 255)

    for i = 1, (self.size.x * self.size.y), 1 do
      sx = (x * self.tile.size.x) * self.scalefx.x
      sy = (y * self.tile.size.y) * self.scalefx.y

      sw = self.tile.size.x * self.scalefx.x
      sh = self.tile.size.y * self.scalefx.y

      local ox, oy = 0, 0

      if self.tiles[i] then
        local tile = self.tiles[i]
        if tile.rotation == ONE_PI then
          oy = 8
        elseif tile.rotation == TWO_PI then
          ox = 8
          oy = 8
        elseif tile.rotation == THREE_PI then
          ox = 8
        end

        local rotation = (tile.rotation * 90) * (3.14/180)
        love.graphics.draw(self.atlas, tile.drawable, sx, sy, rotation, self.scalefx.x, self.scalefx.y, ox, oy)
      end

      if self.shownumbers then
        love.graphics.print(tostring(i), sx, sy)
      end

      if self.showlines then
        love.graphics.rectangle("line", sx, sy, sw, sh)
      end

      local atRowEnd = x == self.size.x - 1
      if atRowEnd then
        x = 0
        y = y + 1
      else
        x = x + 1
      end
    end

    love.graphics.pop()
  end

  return grid
end

return {
  loadBundle = loadBundle,
  generateMap = generateMap,
}
