local UNLOADED = 0
local LOADED = 1
local LOADING = 2

local FADE_IN = 3
local FADE_OUT = 4

frameUnit = 1.0 / 0.0
frame = 255
currentTime = 0

function createStage()
  local stage = {
    state = UNLOADED,
    transition = FADE_IN,
    frame = 0,
    scenes = {
      front = nil,
      back = nil,
    },
  }

  function stage.currentScene(self)
    return self.scenes.front or {
      load = function() end,
      update = function() end,
      draw = function() end,
    }
  end

  function stage.setScene(self, scene)
    self.scenes.back = scene
    self.state = LOADING
  end

  function stage.draw(self)
    self:currentScene().draw()
    print(frame)

    if self.state == LOADING then
      love.graphics.setColor(0, 0, 0, frame)
      love.graphics.rectangle("fill", 0, 0, 600, 600)
    end

    if self.state == UNLOADED then
      love.graphics.clear(0, 0, 255)
      love.graphics.setColor(255, 255, 255)
      love.graphics.print("LOAD A SCENE", 10, 10)
    end
  end

  function stage.update(self, dt)
    if self.state == LOADING then
      if self.transition == FADE_IN then
        frame = 1.0 - (currentTime * frameUnit)
        print(frame)
      end

      if frame <= 0 then
        self.state = LOADED
        frame = 0
        currentTime = 0

        self.scenes.front = self.scenes.back
        self.scenes.back = nil

        self.scenes.front.load()
      end
    elseif self.state == LOADED then
      self:currentScene().update(dt)
    end

    if currentTime == 3.0 then
      currentTime = 0
    end

    currentTime = currentTime + dt
  end

  function stage.load(self)
    love.graphics.setBlendMode("alpha")
  end

  return stage
end

return {
  createStage = createStage,
}
