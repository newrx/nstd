-- list of callbacks

-- scene accessable
-- love.draw
-- love.update
-- love.mousepressed
-- love.mousereleased
-- love.directorydropped
-- love.filedropped
-- love.wheelmoved
-- love.load
-- love.gamepadaxis
-- love.gamepadpressed
-- love.gamepadreleased
-- love.joystickadded
-- love.joystickaxis
-- love.joystickhat
-- love.joystickpressed
-- love.joystickreleased
-- love.joystickremoved
-- love.keypressed
-- love.keyreleased
-- love.mousefocus
-- love.mousemoved
-- love.textedited
-- love.textinput
-- love.touchmoved
-- love.touchpressed
-- love.touchreleased

-- expressed as
-- scene.onInputEvent(event)
-- scene.load()
-- scene.update(deltaTime, width, height)
-- scene.draw(width, height)
-- scene.onExit()

-- lower-engine accessable
-- love.run
-- love.resize
-- love.quit
-- love.visible
-- love.errhand
-- love.errorhandler
-- love.focus
-- love.lowmemory
-- love.displayrotated
-- love.threaderror

function load()
  love.graphics.setNewFont("assets/fonts/virtual_dj/Vdj.ttf", 35)
end

function draw()
  love.graphics.setColor(255, 255, 255) --set the drawing color to red for the ball
  love.graphics.print("No Sleep Til Dawn", 10, 10)
end

function update(dt)

end

return {
  draw = draw,
  update = update,
  load = load,
}
