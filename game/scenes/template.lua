-- list of callbacks

-- scene accessable
-- love.draw
-- love.update
-- love.mousepressed
-- love.mousereleased
-- love.directorydropped
-- love.filedropped
-- love.wheelmoved
-- love.load
-- love.gamepadaxis
-- love.gamepadpressed
-- love.gamepadreleased
-- love.joystickadded
-- love.joystickaxis
-- love.joystickhat
-- love.joystickpressed
-- love.joystickreleased
-- love.joystickremoved
-- love.keypressed
-- love.keyreleased
-- love.mousefocus
-- love.mousemoved
-- love.textedited
-- love.textinput
-- love.touchmoved
-- love.touchpressed
-- love.touchreleased

-- expressed as
-- scene.onInputEvent(event)
-- scene.load()
-- scene.update(deltaTime, width, height)
-- scene.draw(width, height)
-- scene.onExit()

-- lower-engine accessable
-- love.run
-- love.resize
-- love.quit
-- love.visible
-- love.errhand
-- love.errorhandler
-- love.focus
-- love.lowmemory
-- love.displayrotated
-- love.threaderror

local mapbuilder = require("mapbuilder")

local env = "dev"
local map

local objects = {}
local x, y = 0, 0

function load()
  love.graphics.setNewFont("assets/fonts/virtual_dj/Vdj.ttf", 12)

  local mapBundle = mapbuilder.loadBundle("assets/maps/default")
  map = mapbuilder.generateMap(mapBundle)

  love.window.setMode(map:getSizePx())

  -- physics
  love.physics.setMeter(18)
  -- create a world for the bodies to exist in with horizontal gravity of 0 and vertical gravity of 9.81
  world = love.physics.newWorld(0, 9.81*18*2, true)

  --let's create a ball
  objects.ball = {}
  objects.ball.body = love.physics.newBody(world, 650/2, 0, "dynamic") --place the body in the center of the world and make it dynamic, so it can move around
  objects.ball.shape = love.physics.newCircleShape( 20) --the ball's shape has a radius of 20
  objects.ball.fixture = love.physics.newFixture(objects.ball.body, objects.ball.shape, 1) -- Attach fixture to body and give it a density of 1.
  objects.ball.fixture:setRestitution(0.9) --let the ball bounce

  objects.ground = {}
  objects.ground.body = love.physics.newBody(world, 0, 0) --remember, the shape (the rectangle we create next) anchors to the body from its center, so we have to move it to (650/2, 650-50/2)
  objects.ground.shape = love.physics.newChainShape(false, 141, 101, 144,  291, 187, 239, 189, 430, 341, 428, 338, 289, 671, 291, 673, 145)--make a rectangle with a width of 650 and a height of 50
  objects.ground.fixture = love.physics.newFixture(objects.ground.body, objects.ground.shape); --attach shape to body
end

function draw()
  map:draw(0, 0)

  love.graphics.setColor(0.76, 0.18, 0.05) --set the drawing color to red for the ball
  love.graphics.circle("fill", objects.ball.body:getX(), objects.ball.body:getY(), objects.ball.shape:getRadius())

  love.graphics.print(tostring(x) .. "x" .. tostring(y), 10, 10)
  love.graphics.print("Current FPS: "..tostring(love.timer.getFPS()), 10, 30)
end

function update(dt)
  world:update(dt)

  if love.keyboard.isDown("right") then --press the right arrow key to push the ball to the right
    objects.ball.body:applyForce(2000, 0)
  elseif love.keyboard.isDown("left") then --press the left arrow key to push the ball to the left
    objects.ball.body:applyForce(-2000, 0)
  elseif love.keyboard.isDown("up") then --press the up arrow key to set the ball in the air
    objects.ball.body:setPosition(650/2, 650/2)
    objects.ball.body:setLinearVelocity(0, 0) --we must set the velocity to zero to prevent a potentially large velocity generated by the change in position
  end
end

function love.mousemoved(mx, my)
  x = mx
  y = my
end

function love.keypressed(key)
  if env == "dev" then
    if key == "l" then
      map:toggleLines()
    end

    if key == "n" then
      map:toggleNumberLabels()
    end

    if key == "escape" or key == "q" then
      love.event.quit()
    end

    if key == "r" then
      love.event.quit("restart")
    end
  end
end

return {
  draw = draw,
  update = update,
  load = load,
}
